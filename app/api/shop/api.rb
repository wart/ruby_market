module Shop
  class API < Grape::API
    version 'v1', using: :path, vendor: 'cyber'
    format :json
    # http://althafhameez.com/blog/2013/07/10/authenticating-an-api-with-grape-and-devise-in-rails-4/
    #=================================================================
    helpers ApiHeplers
    #=================================================================
    mount Shop::API_layer_param_category
    mount Shop::API_layer_param_type
    mount Shop::API_layer_prod_relation_type
    mount Shop::API_layer_relation_type
    mount Shop::API_layer
    mount Shop::API_product_category
    mount Shop::API_product_manufacturer
    mount Shop::API_product_param_category
    mount Shop::API_product_param_type
    mount Shop::API_product_type
    mount Shop::API_product
    
    
    get '/test' do
     
      "all ok"  
    end
    
    #add_swagger_documentation
    #=================================================================
  end
end