module Shop
  module ApiHeplers

=begin
    def current_user
        @current_user ||= User.new(env)
        nil
    end
=end
   
    
       def warden
          env['warden']
        end

        def authenticated?
          if warden.authenticated?
            return true
          else
            error!({"error" => "Unauthorized 401"}, 401)
          end
        end

        def current_user
          warden.user
        end
    
    
    
    def authenticate!(model,action)
#       error!('401 Unauthorized', 401) unless authenticated?
#       error!('401 Unauthorized for role', 401) unless ::Ability.new(current_user).can?(action,model)
     
    end
    
    #------------------------------------------------
    def mdl_to_name(a)
      a.to_s.underscore.split('_').map { |i| i.capitalize}.join(' ')
    end
    #------------------------------------------------
    def cNamNote(mdl)
        authenticate!(mdl,:create)
        unless mdl.find_by_name(params[:name]).nil?
          error!(
          {
              "error" => "#{mdl_to_name(mdl)} with name \"#{params[:name]}\" exists"
          }, 400)
        end
        @obj=mdl.create!(name: params[:name],note: params[:note])
        {
          mdl.to_s.underscore.to_sym=>@obj
        }      
    end
    #------------------------------------------------
    def uNamNote(mdl)
        authenticate!(mdl,:update)
        @obj=mdl.find_by_id(params[:id])
        @mn=mdl_to_name(mdl)
        if @obj.nil?
          error!(
          {
            "error" => "#{@mn} with id: #{params[:id]} not found"
          }, 400);
        end
        @wn=mdl.where(:name=>params[:name]).where.not(:id=>params[:id]).first
        unless @wn.nil?
          error!(
          {
            "error" => "#{@mn} with name \"#{params[:name]}\" exists"
          }, 400);
        end
        @obj.name=params[:name]
        @obj.note=params[:note]
        @obj.save
        {
          mdl.to_s.underscore.to_sym=>@obj
        }           
    end
    #------------------------------------------------
    def dNameNote(mdl)
        authenticate!(mdl,:destroy)
        @obj=mdl.find_by_id(params[:id])
        if @obj.nil?
          error!(
          {
            "error" => "#{mdl_to_name(mdl)} with id: #{params[:id]} not found"
          }, 400)
        end
        @obj.destroy
        {
          :result=>"deleted"
        }       
    end
    #------------------------------------------------
  end
end