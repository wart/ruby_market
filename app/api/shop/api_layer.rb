module Shop
  class API_layer < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :layer do
      #-----------------------------------------------
      desc "Create a Layer."
      params do
        requires :name, type: String, desc: "Layer Name"
        optional :note, type: String, desc: "Layer Note"
      end
      post do
        cNamNote Layer      
      end
      #-----------------------------------------------
      desc "Update a Layer."
      params do
        requires :id,   type: Integer, desc: "Layer ID."
        requires :name, type: String,  desc: "Layer Name"
        optional :note, type: String,  desc: "Layer Note"        
      end
      put ':id' do
        uNamNote Layer
      end
      #-----------------------------------------------
      desc "Delete a Layer."
      params do
        requires :id, type: Integer, desc: "Layer ID."
      end
      delete ':id' do
        dNameNote Layer      
      end
      #-----------------------------------------------
      desc "Add a Layer Relation."
      params do
        requires :parent_id             , type: Integer, desc: "Parent Layer ID."
        requires :child_id              , type: Integer, desc: "Child Layer ID."
        requires :layer_relation_type_id, type: Integer, desc: "Layer Relation Type ID"
      end
      post ':parent_id/add/relation' do
        authenticate!(LayerRelation,:create)
        
      end
      #-----------------------------------------------
      desc "Delete a Layer Relation."
      params do
        requires :parent_id             , type: Integer, desc: "Parent Layer ID."
        requires :child_id              , type: Integer, desc: "Child Layer ID."
        requires :layer_relation_type_id, type: Integer, desc: "Layer Relation Type ID"
      end
      delete ':parent_id/del/relation' do
        authenticate!(LayerRelation,:destroy)
        
      end
      #-----------------------------------------------
      helpers do      
        
        def add_lay_link(mdl,mdl_link)
          authenticate!(mdl_link,:create)
          
          @il=:layer_id
          @ir=:layer_prod_relation_type_id
          @io=(mdl.to_s.underscore+"_id").to_sym
          @mn=mdl_to_name(mdl)
          
          unless mdl_link.where(@il => params[@il],
                                @ir => params[@ir],
                                @io => params[@io]).first.nil?
            error!(
            {
              "error" => "Link of Layer and #{@mn} with Relation Type (#{params[@ir]}) was exists"
            }, 400)
          end
          
          @layer=Layer.find_by_id(params[@il])
           
          if @layer.nil?
            error!(
            {
              "error" => "Layer with id: #{params[@il]} not found"
            }, 400)
          end
          
          @rel_type=LayerProdRelationType.find_by_id(params[@ir])
          
          if @rel_type.nil?
            error!(
            {
              "error" => "Layer Prod Relation Type with id: #{params[@ir]} not found"
            }, 400)
          end
          
          @subj=mdl.find_by_id(params[@io])
          
          if @subj.nil?
            error!(
            {
              "error" => "#{@mn} with id: #{params[@io]} not found"
            }, 400)
          end
          if block_given?
            yield
          else
            mdl_link.create!(@il => params[@il],@ir => params[@ir],@io=>params[@io])
            {
              mdl.to_s.underscore.to_sym =>
              {
                @il => params[@il],
                @ir => params[@ir],
                @io => params[@io]
              }
            }
          end
        end
        #--------------------------------------
        def del_or_some_lay_link(mdl,mdl_link,action)
          @realaction= :destroy
          if block_given?
            @realaction=action
          end
          @realaction= :destroy
          authenticate! mdl_link, action
          @il=:layer_id
          @ir=:layer_prod_relation_type_id
          @io=(mdl.to_s.underscore+"_id").to_sym
          @mn=mdl_to_name(mdl)          
          
          
          @link=mdl_link.where(@il => params[@il],
                               @ir => params[@ir],
                               @io => params[@io]).first
          if @link.nil? 
            error!(
            {
              "error" => "Link of Layer and #{@mn} with Relation Type (#{params[@ir]}) not exists"
            }, 400)
          end
          if block_given?
            yield @link
          else                    
            @link.destroy
            {
              :result=>"deleted"
            }
          end
        end          
      end  
      #-----------------------------------------------            
      desc "Add Product Category to Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_category_id         , type: Integer, desc: "Product Category ID"
      end
      post ':layer_id/add/product_category' do
        add_lay_link ProductCategory, LayerProductCategory
      end
      #-----------------------------------------------
      desc "Delete Product Category from Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_category_id         , type: Integer, desc: "Product Category ID"
      end
      delete ':layer_id/del/product_category' do
        del_or_some_lay_link ProductCategory, LayerProductCategory
      end
      #-----------------------------------------------     
      desc "Add Product Manufacturer to Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_manufacturer_id     , type: Integer, desc: "Product Manufacturer ID"
      end
      post ':layer_id/add/product_manufacturer' do
        add_lay_link ProductManufacturer, LayerProductManufacturer
      end
      #-----------------------------------------------
      desc "Delete Product Manufacturer from Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_manufacturer_id     , type: Integer, desc: "Product Manufacturer ID"
      end
      delete ':layer_id/del/product_manufacturer' do
        del_or_some_lay_link ProductManufacturer, LayerProductManufacturer
      end
      #-----------------------------------------------     
      desc "Add Product Param to Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_param_type_id       , type: Integer, desc: "Product Param Type ID"
        optional :by_val                      , type: Integer, desc: "Flags using value comparation"
        optional :val                         , type: String,  desc: "Comparation Value"    
      end
      post ':layer_id/add/product_param' do
        add_lay_link ProductParamType, LayerProductParam do
          @a={
            :layer_id                    => params[:layer_id],
            :layer_prod_relation_type_id => params[:layer_prod_relation_type_id],
            :product_param_type_id       => params[:product_param_type_id],
            :by_val                      => params[:by_val],
            :val                         => params[:val]
          }
          mdl_link.create! *@a
          {
            :layer_product_param => @a
          }          
        end
      end
      #-----------------------------------------------
      desc "Update Product Param in Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_param_type_id       , type: Integer, desc: "Product Param Type ID"
        optional :by_val                      , type: Integer, desc: "Flags using value comparation"
        optional :val                         , type: String,  desc: "Comparation Value"
      end
      post ':layer_id/upd/product_param' do
        del_or_some_lay_link ProductParamType, LayerProductParam, :update do
          @obj.by_val = params[:by_val]
          @obj.val    = params[:val]
          @obj.save
          {
            :layer_product_param => @obj
          }                   
        end
      end
      #-----------------------------------------------         
      desc "Delete Product Param from Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_param_type_id       , type: Integer, desc: "Product Param Type ID"
      end
      delete ':layer_id/del/product_param' do
        del_or_some_lay_link ProductParamType, LayerProductParam 
      end
      #-----------------------------------------------     
      desc "Add Product Type to Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_type_id             , type: Integer, desc: "Product Type ID"
      end
      post ':layer_id/add/product_type' do
        add_lay_link ProductType, LayerProductType
      end
      #-----------------------------------------------
      desc "Delete Product Type from Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_type_id             , type: Integer, desc: "Product Type ID"
      end
      delete ':layer_id/del/product_type' do
        del_or_some_lay_link ProductType, LayerProductType
      end
      #-----------------------------------------------   
      desc "Add Product to Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_id                  , type: Integer, desc: "Product ID"
      end
      post ':layer_id/add/product' do
        add_lay_link Product, LayerProduct
      end
      #-----------------------------------------------
      desc "Delete Product from Layer."
      params do
        requires :layer_id                    , type: Integer, desc: "Layer ID."
        requires :layer_prod_relation_type_id , type: Integer, desc: "Layer Prod Relation Type ID."
        requires :product_id                  , type: Integer, desc: "Product ID"
      end
      delete ':layer_id/del/product' do
        del_or_some_lay_link Product, LayerProduct
      end
      #-----------------------------------------------   
    end
    #=================================================================        
  end
end