module Shop
  class API_layer_param_category < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :layer_param_category do
      #-----------------------------------------------
      desc "Create a Layer Param Category."
      params do
        requires :name, type: String, desc: "Layer Param Category Name"
        optional :note, type: String, desc: "Layer Param Category Note"
      end
      post do
        cNamNote LayerParamCategory
      end
      #-----------------------------------------------
      desc "Update a Layer Param Category."
      params do
        requires :id,   type: Integer, desc: "Layer Param Category ID."
        requires :name, type: String,  desc: "Layer Param Category Name"
        optional :note, type: String,  desc: "Layer Param Category Note"
      end
      put ':id' do
        uNamNote LayerParamCategory
      end
      #-----------------------------------------------
      desc "Delete a Layer Param Category."
      params do
        requires :id, type: Integer, desc: "Layer Param Category ID."
      end
      delete ':id' do
        dNamNote LayerParamCategory
      end
      #-----------------------------------------------
     end
    #=================================================================
  end
end