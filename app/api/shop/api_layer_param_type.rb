module Shop
  class API_layer_param_type < Grape::API
    helpers ApiHeplers 
    #=================================================================
   resource :layer_param_type do
      #-----------------------------------------------
      desc "Create a Layer Param Type."
      params do
        requires :layer_param_category_id, type: Integer, desc: "Layer Param Category ID."
        requires :name, type: String, desc: "Layer Param Type Name"
        optional :note, type: String, desc: "Layer Param Type Note"
      end
      post do
        authenticate!(LayerParamType,:create)
        
      end
      #-----------------------------------------------
      desc "Update a Layer Param Type."
      params do
        requires :id,   type: Integer, desc: "Layer Param Type ID."
        requires :name, type: String,  desc: "Layer Param Type Name"
        optional :note, type: String,  desc: "Layer Param Type Note"        
      end
      put ':id' do
        authenticate!(LayerParamType,:update)
        
      end
      #-----------------------------------------------
      desc "Delete a Layer Param Type."
      params do
        requires :id, type: Integer, desc: "Layer Param Type ID."
      end
      delete ':id' do
        authenticate!(LayerParamType,:destroy)
        
      end
      #-----------------------------------------------      
      desc "Update a Layer Param Type parent Category."
      params do
        requires :id, type: Integer, desc: "Layer Param Type ID."
        requires :layer_param_category_id, type: Integer, desc: "Layer Param Category ID."
      end
      put ':id/update_layer_param_category' do
        authenticate!(LayerParamType,:update)
        
      end      
      #-----------------------------------------------      
    end    
    #=================================================================        
  end
end