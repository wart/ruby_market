module Shop
  class API_layer_prod_relation_type < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :layer_prod_relation_type do
     #-----------------------------------------------
      desc "Create a Layer Product Relation Type."
      params do
        requires :name, type: String, desc: "Layer Product Relation Type Name"
        optional :note, type: String, desc: "Layer Product Relation Type Note"
      end
      post do
        cNamNote LayerProdRelationType
      end
      #-----------------------------------------------
      desc "Update a Layer Product Relation Type."
      params do
        requires :id,   type: Integer, desc: "Layer Product Relation Type ID."
        requires :name, type: String,  desc: "Layer Product Relation Type Name"
        optional :note, type: String,  desc: "Layer Product Relation Type Note"        
      end
      put ':id' do
        uNamNote LayerProdRelationType          
      end
      #-----------------------------------------------
      desc "Delete a Layer Product Relation Type."
      params do
        requires :id, type: Integer, desc: "Layer Product Relation Type ID."
      end
      delete ':id' do
        dNamNote LayerProdRelationType          
      end
      #-----------------------------------------------      
    end
    #=================================================================        
  end
end