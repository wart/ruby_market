module Shop
  class API_layer_relation_type < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :layer_relation_type do
      #-----------------------------------------------
      desc "Create a Layer Relation Type."
      params do
        requires :name, type: String, desc: "Layer Relation Type Name"
        optional :note, type: String, desc: "Layer Relation Type Note"
      end
      post do
        cNamNote LayerRelationType
      end
      #-----------------------------------------------
      desc "Update a Layer Product Relation Type."
      params do
        requires :id,   type: Integer, desc: "Layer Relation Type ID."
        requires :name, type: String,  desc: "Layer Relation Type Name"
        optional :note, type: String,  desc: "Layer Relation Type Note"        
      end
      put ':id' do
        uNamNote LayerRelationType
      end
      #-----------------------------------------------
      desc "Delete a Layer Relation Type."
      params do
        requires :id, type: Integer, desc: "Layer Relation Type ID."
      end
      delete ':id' do
        dNamNote LayerRelationType
      end
      #-----------------------------------------------       
    end
    
    #=================================================================        
  end
end