module Shop
  class API_product < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :product do
       #-----------------------------------------------
      desc "Create a Product."
      params do
        requires :product_manufacturer_id, type: Integer, desc: "Product Manufacturer ID."
        requires :product_type_id,         type: Integer, desc: "Product Type ID."        
        requires :name,                    type: String,  desc: "Product Name"
        optional :text,                    type: String,  desc: "Product Text"
      end
      post do
        authenticate!(Product,:create)
        
      end
      #-----------------------------------------------
      desc "Update a Product."
      params do
        requires :id,      type: Integer, desc: "Product ID"
        requires :name,    type: String,  desc: "Product Name"
        optional :text,    type: String,  desc: "Product Text"       
      end
      put ':id' do
        authenticate!(Product,:update)
        
      end
      #-----------------------------------------------
      desc "Delete a Product."
      params do
        requires :id, type: Integer, desc: "Product ID."
      end
      delete ':id' do
        authenticate!(Product,:destroy)
        
      end
      #-----------------------------------------------
     desc "Update a Manufactorer of Product."
      params do
        requires :id, type: Integer, desc: "Product ID."
        requires :product_manufacturer_id, type: Integer, desc: "Product Manufactorer ID."
      end
      put ':id/manufactorer' do
        authenticate!(Product,:update)
        
      end
      #-----------------------------------------------
      desc "Update a Type of Product."
      params do
        requires :id, type: Integer, desc: "Product ID."
        requires :product_type_id, type: Integer, desc: "Product Type ID."
      end
      put ':id/type' do
        authenticate!(Product,:update)
        
      end                  
      #-----------------------------------------------      
      desc "Update a Product Params."
      params do
        requires :id, type: Integer, desc: "Product ID."
      end
      put ':id/params' do
        authenticate!(ProductParam,:update)
        
      end                  
      #-----------------------------------------------      
    end    
    #=================================================================        
  end
end
