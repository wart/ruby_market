module Shop
  class API_product_category < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :product_category do
      #-----------------------------------------------
      desc "Create a Product Category."
      params do
        requires :parent_id, type: Integer, desc: "Parent Product Category ID"
        requires :name     , type: String,  desc: "Product Category Name"
        optional :shortname, type: String,  desc: "Product Category ShortName"
      end
      post do
        authenticate!(ProductCategory,:create)
        
      end
      #-----------------------------------------------
      desc "Update a Product Category."
      params do
        requires :id,        type: Integer, desc: "Product Category ID."
        requires :name,      type: String,  desc: "Product Category Name"
        optional :shortname, type: String,  desc: "Product Category ShortName"        
      end
      put ':id' do
        authenticate!(ProductCategory,:update)
        
      end
      #-----------------------------------------------
      desc "Delete a Product Category."
      params do
        requires :id, type: Integer, desc: "Product Category ID."
      end
      delete ':id' do
        authenticate!(ProductCategory,:destroy)
        
      end
      #-----------------------------------------------      
      desc "Update a Parent Product Category."
      params do
        requires :id       , type: Integer, desc: "Product Category ID."
        requires :parent_id, type: Integer, desc: "Parent Product Category ID"
      end
      put ':id/upd/parent' do
        authenticate!(ProductCategory,:update)
        
      end      
      #-----------------------------------------------            
    end
    #=================================================================        
  end
end