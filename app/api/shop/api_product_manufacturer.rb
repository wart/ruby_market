module Shop
  class API_product_manufacturer < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :product_manufacturer do
     #-----------------------------------------------
      desc "Create a Product Manufacturer."
      params do
        requires :name, type: String, desc: "Product Manufacturer Name"
        optional :note, type: String, desc: "Product Manufacturer Note"
      end
      post do
        cNamNote ProductManufacturer         
      end
      #-----------------------------------------------
      desc "Update a Product Manufacturer."
      params do
        requires :id,   type: Integer, desc: "Product Manufacturer ID."
        requires :name, type: String,  desc: "Product Manufacturer Name"
        optional :note, type: String,  desc: "Product Manufacturer Note"       
      end
      put ':id' do
        uNamNote ProductManufacturer
      end
      #-----------------------------------------------
      desc "Delete a Product Manufacturer."
      params do
        requires :id, type: Integer, desc: "Product Manufacturer ID."
      end
      delete ':id' do
        dNamNote ProductManufacturer          
      end
      #-----------------------------------------------
      desc "Add Product Type to Product Manufacturer"
      params do
        requires :id              , type: Integer, desc: "Manufacturer ID."
        requires :product_type_id , type: Integer, desc: "Product Type ID."
      end
      post ':id/add/product_type' do
        authenticate!(ProductManLinkType,:create)
        @man=ProductManufacturer.find_by_id(params[:id])
        if @man.nil? ; error!({"error" => "Product Manufacturer with id: #{params[:id]} not found"}, 400); end
        @typ=ProductType.find_by_id(params[:product_type_id])
        if @typ.nil? ; error!({"error" => "Product Type with id: #{params[:product_type_id]} not found"}, 400); end
        unless ProductManLinkType.where(:product_manufacturer_id=>params[:id], :product_type_id=>params[:product_type_id]).first.nil?
          error!({"error" => "Link of Product Manufacturer(#{params[:id]}) and Type(#{params[:product_type_id]}) was exists"}, 400);
        end
        @obj= ProductManLinkType.create!(:product_manufacturer_id =>@man.id, :product_type_id=> @typ.id)
        {:product_man_link_type=>{:product_manufacturer_id=>@man.id, :product_type_id=>@typ.id}}
      end
      #-----------------------------------------------
      desc "Delete Product Type from Product Manufacturer"
      params do
        requires :id              , type: Integer, desc: "Manufacturer ID."
        requires :product_type_id , type: Integer, desc: "Product Type ID."
      end
      delete ':id/del/product_type' do
        authenticate!(ProductManLinkType,:destroy)
        @obj=ProductManLinkType.where(:product_manufacturer_id=>params[:id], :product_type_id=>params[:product_type_id]).first
        if @obj.nil?
          error!({"error" => "Link of Product Manufacturer(#{params[:id]}) and Type(#{params[:product_type_id]}) not exists."}, 400);
        end
        @obj.destroy
        {:result=>"deleted"}        
      end
      #-----------------------------------------------        
    end
    #=================================================================        
  end
end