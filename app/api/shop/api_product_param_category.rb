module Shop
  class API_product_param_category < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :product_param_category do
      #-----------------------------------------------
      desc "Create a Product Param Category."
      params do
        requires :name, type: String,  desc: "Product Param Category Name"
        optional :note, type: String,  desc: "Product Param Category Note"      
      end
      post do
        cNamNote ProductParamCategory    
      end
      #-----------------------------------------------
      desc "Update a Product Param Category."
      params do
        requires :id,   type: Integer, desc: "Product Param Category ID."
        requires :name, type: String,  desc: "Product Param Category Name"
        optional :note, type: String,  desc: "Product Param Category Note"
      end
      put ':id' do
        uNamNote ProductParamCategory         
      end
      #-----------------------------------------------
      desc "Delete a Product Param Category."
      params do
        requires :id, type: Integer, desc: "Product Param Category ID."
      end
      delete ':id' do
        dNamNote ProductParamCategory        
      end
      #-----------------------------------------------      
    end    
    #=================================================================        
  end
end