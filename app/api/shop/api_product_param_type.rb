module Shop
  class API_product_param_type < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :product_param_type do
      #-----------------------------------------------
      desc "Create a Product Param Type."
      params do
        requires :product_param_category_id, type: Integer, desc: "Product Param Category ID."
        requires :name,                      type: String,  desc: "Product Param Type Name"
        requires :is_main,                   type: String,  desc: "Product Param Type FlagMain"
        optional :note,                      type: String,  desc: "Product Param Type Note"
      end
      post do
        authenticate!(ProductParamType,:create)
        
      end
      #-----------------------------------------------
      desc "Update a Product Param Type."
      params do
        requires :id,      type: Integer, desc: "Product Param Type ID."
        requires :name,    type: String,  desc: "Product Param Type Name"
        requires :is_main, type: String,  desc: "Product Param Type FlagMain"
        optional :note,    type: String,  desc: "Product Param Type Note"        
      end
      put ':id' do
        authenticate!(ProductParamType,:update)
        
      end
      #-----------------------------------------------
      desc "Delete a Product Param Type."
      params do
        requires :id, type: Integer, desc: "Product Param Type ID."
      end
      delete ':id' do
        authenticate!(ProductParamType,:destroy)
        
      end
      #-----------------------------------------------      
      desc "Update a Product Param Type parent Category."
      params do
        requires :id,                        type: Integer, desc: "Product Param Type ID."
        requires :product_param_category_id, type: Integer, desc: "Product Param Category ID."
      end
      put ':id/update_product_param_category' do
        authenticate!(ProductParamType,:update)
        
      end      
      #-----------------------------------------------        
    end    
    #=================================================================        
  end
end