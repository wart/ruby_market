module Shop
  class API_product_type < Grape::API
    helpers ApiHeplers 
    #=================================================================
    resource :product_type do
       #-----------------------------------------------
      desc "Create a Product Type."
      params do
        requires :product_category_id, type: Integer, desc: "Product Type Category ID."
        requires :parent_type_id,      type: Integer, desc: "Product Type Parent Type ID."        
        requires :name,                type: String,  desc: "Product Type Name"
        optional :note,                type: String,  desc: "Product Type Note"
      end
      post do
        authenticate!(ProductType,:create)
        
      end
      #-----------------------------------------------
      desc "Update a Product Type."
      params do
        requires :id,      type: Integer, desc: "Product Type ID."
        requires :name,    type: String,  desc: "Product Type Name"
        optional :note,    type: String,  desc: "Product Type Note"       
      end
      put ':id' do
        authenticate!(ProductType,:update)
        
      end
      #-----------------------------------------------
      desc "Delete a Product Type."
      params do
        requires :id, type: Integer, desc: "Product Type ID."
      end
      delete ':id' do
        authenticate!(ProductType,:destroy)
        
      end
      #-----------------------------------------------
      desc "Update a Product Type Parent."
      params do
        requires :id, type: Integer, desc: "Product Type ID."
        requires :parent_id, type: Integer, desc: "Parent Product Type ID."
      end
      put ':id/parent' do
        authenticate!(ProductType,:update)
        
      end
      #-----------------------------------------------
      desc "Update a Product Type Category."
      params do
        requires :id, type: Integer, desc: "Product Type ID."
        requires :product_category_id, type: Integer, desc: "Product Category ID."
      end
      put ':id/category' do
        authenticate!(ProductType,:update)
        
      end      
      #-----------------------------------------------
      desc "Add Product Param Categoty to a Product Type."
      params do
        requires :id, type: Integer, desc: "Product Type ID."
        requires :product_param_category_id, type: Integer, desc: "Product Param Category ID."
      end
      put ':id/add/param_category' do
        authenticate!(ProductTypeVsParamCat,:create)
        
      end
      #-----------------------------------------------
      desc "Delete Product Param Categoty from a Product Type."
      params do
        requires :id, type: Integer, desc: "Product Type ID."
        requires :product_param_category_id, type: Integer, desc: "Product Param Category ID."
      end
      delete ':id/del/param_category' do
        authenticate!(ProductTypeVsParamCat,:destroy)
        
      end
      #-----------------------------------------------
    end    
    #=================================================================        
  end
end