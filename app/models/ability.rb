class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
   
    alias_action :create, :read, :update, :destroy, :to => :crud
    if user.role? :mngr_product_type               ; can :crud, ProductType             ; end;
    if user.role? :mngr_product_category           ; can :crud, ProductCategory         ; end;
    if user.role? :mngr_product_manufacturer       ; can :crud, ProductManufacturer     ; end;
    if user.role? :mngr_product_param_category     ; can :crud, ProductParamCategory    ; end;
    if user.role? :mngr_product_param_type         ; can :crud, ProductParamType        ; end;
    if user.role? :mngr_product_param              ; can :crud, ProductParam            ; end;
    if user.role? :mngr_product_man_link_type      ; can :crud, ProductManLinkType      ; end;
    if user.role? :mngr_product_type_vs_param_cat  ; can :crud, ProductTypeVsParamCat   ; end;
    if user.role? :mngr_product                    ; can :crud, Product                 ; end;
    if user.role? :mngr_layer_param_category       ; can :crud, LayerParamCategory      ; end;
    if user.role? :mngr_layer_param_type           ; can :crud, LayerParamType          ; end;
    if user.role? :mngr_layer_param                ; can :crud, LayerParam              ; end;
    if user.role? :mngr_layer_relation_type        ; can :crud, LayerRelationType       ; end;
    if user.role? :mngr_layer_relation             ; can :crud, LayerRelation           ; end;
    if user.role? :mngr_layer_prod_relation_type   ; can :crud, LayerProdRelationType   ; end;
    if user.role? :mngr_layer_product_category     ; can :crud, LayerProductCategory    ; end;
    if user.role? :mngr_layer_product_manufacturer ; can :crud, LayerProductManufacturer; end;
    if user.role? :mngr_layer_product_param        ; can :crud, LayerProductParam       ; end;
    if user.role? :mngr_layer_product_type         ; can :crud, LayerProductType        ; end;
    if user.role? :mngr_layer_product              ; can :crud, LayerProduct            ; end;
    if user.role? :mngr_layer                      ; can :crud, Layer                   ; end;
    if user.role? :admin                           ; can :crud, :all                    ; end;
#    can :update, Post do |post| post.try(:user) == user end

     # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
