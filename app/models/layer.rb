# encoding: utf-8
class Layer < ActiveRecord::Base
  
  has_many :child_links , class_name: "LayerRelation", foreign_key: "parent_id", inverse_of: :parent
  has_many :parent_links, class_name: "LayerRelation", foreign_key: "child_id" , inverse_of: :child
  
  has_many :parents , class_name: "Layer", through: :parent_links
  has_many :childs  , class_name: "Layer", through: :child_links

  has_many :layer_params, inverse_of: :layer

  has_many :layer_product_categories, inverse_of: :layer
  has_many :layer_product_params, inverse_of: :layer
  has_many :layer_product_types, inverse_of: :layer
  has_many :layer_products, inverse_of: :layer
  has_many :layer_product_manufacturers, inverse_of: :layer
  
  has_many :product_categories, through: :layer_product_categories
  has_many :product_param_types, through: :layer_product_params
  has_many :product_types, through: :layer_product_types
  has_many :products, through: :layer_products
  has_many :product_manufacturers, through: :layer_product_manufacturers  
  
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer:
        zero:  "слоя"
        one:   "слой"
        few:   "слоя"
        many:  "слоев"
        other: "слоев"
    attributes:
      layer:
        name:                        "Наименование"
        note:                        "Примечание"
        child_links:                 "Связки c потомками"
        parent_links:                "Связки c предками"
        parents:                     "Предки"
        childs:                      "Потомки"
        layer_params:                "Параметры"
        layer_product_categories:    "Связки к подчиненным категориям товаров"
        layer_product_params:        "Связки к подчиненным типам параметров"
        layer_product_types:         "Связки к подчиненным типам товаров"
        layer_products:              "Связки к подчиненным товарам"
        layer_product_manufacturers: "Связки к подчиненным производителям"
        product_categories:          "Подчиненные категории товаров"
        product_param_types:         "Подчиненные типы параметров"
        product_types:               "Подчиненные типы товаров"
        products:                    "Подчиненные товары"
        product_manufacturers:       "Подчиненные производители"
  placeholder:
      layer:
        name:                        "Введите наименование"
        note:                        "Введите примечание"
        child_links:                 "Добавьте связку c потомком"
        parent_links:                "Добавьте связку c предком"
        parents:                     "Добавьте предка"
        childs:                      "Добавьте потомка"
        layer_params:                "Добавьте параметр"
        layer_product_categories:    "Добавьте связку к подчиненным категориям товаров"
        layer_product_params:        "Добавьте связку к подчиненным типам параметров"
        layer_product_types:         "Добавьте связку к подчиненным типам товаров"
        layer_products:              "Добавьте связку к подчиненным товарам"
        layer_product_manufacturers: "Добавьте связку к подчиненным производителям"
        product_categories:          "Добавьте подчиненную категорию товаров"
        product_param_types:         "Добавьте подчиненный тип параметров"
        product_types:               "Добавьте подчиненный тип товаров"
        products:                    "Добавьте подчиненный товар"
        product_manufacturers:       "Добавьте подчиненного производителя"
  confirm_delete:
    layer: 'Вы действительно хотите удалить слой %{name}?'
STR
