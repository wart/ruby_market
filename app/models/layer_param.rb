# encoding: utf-8
class LayerParam < ActiveRecord::Base
    belongs_to :layer, inverse_of: :layer_params
    belongs_to :layer_param_type, inverse_of: :layer_params
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_param:
        zero:  "параметров"
        one:   "параметр"
        few:   "параметра"
        many:  "параметров"
        other: "параметров"
    attributes:
      layer_param:
        layer:            "Слой"
        layer_param_type: "Тип параметра"
        val:              "Значение"
  placeholder:
      layer_param:
        layer:            "Выберите слой"
        layer_param_type: "Выберите тип параметра"
        val:              "Введите значение"
  confirm_delete:
    layer_param: 'Вы действительно хотите удалить параметр слоя?'
STR
