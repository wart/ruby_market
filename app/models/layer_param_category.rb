# encoding: utf-8
class LayerParamCategory < ActiveRecord::Base
  
  has_many :layer_param_types, inverse_of: :layer_param_category
   
  attr_accessible :name, :note
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_param_category:
        zero:  "категорий типов параметров"
        one:   "категория типов параметров"
        few:   "категории типов параметров"
        many:  "категорий типов параметров"
        other: "категорий типов параметров"
    attributes:
      layer_param_category:
        name:              "Наименование"
        note:              "Примечание"
        layer_param_types: "Типы параметров"
  placeholder:
      layer_param_category:
        name:              "Введите наименование"
        note:              "Введите примечание"
        layer_param_types: "Добавьте типы параметров"
  confirm_delete:
    layer_param_category: 'Вы действительно хотите удалить категорию %{name}?'
STR


