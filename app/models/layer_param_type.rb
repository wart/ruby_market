# encoding: utf-8
class LayerParamType < ActiveRecord::Base
  
  belongs_to :layer_param_category, inverse_of: :layer_param_types
  has_many :layer_params, inverse_of: :layer_param_type

  
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_param_type:
        zero:  "типов параметров"
        one:   "тип параметров"
        few:   "типа параметров"
        many:  "типов параметров"
        other: "типов параметров"
    attributes:
      layer_param_type:
        name:                 "Наименование"
        note:                 "Примечание"
        layer_param_category: "Категория"
        layer_params:         "Параметры слоев"
  placeholder:
      layer_param_type:
        name:                 "Введите Наименование"
        note:                 "Введите Примечание"
        layer_param_category: "Выберите категорию"
        layer_params:         "Создайте параметры слоев"
  confirm_delete:
    layer_param_type: 'Вы действительно хотите удалить тип параметров %{name}?'
STR
