# encoding: utf-8
class LayerProdRelationType < ActiveRecord::Base
  has_many :layer_product_categories, inverse_of: :layer_prod_relation_type
  has_many :layer_product_params, inverse_of: :layer_prod_relation_type
  has_many :layer_product_types, inverse_of: :layer_prod_relation_type
  has_many :layer_products, inverse_of: :layer_prod_relation_type
  has_many :layer_product_manufacturers, inverse_of: :layer_prod_relation_type
  
  has_many :product_categories, through: :layer_product_categories
  has_many :product_param_types, through: :layer_product_params
  has_many :product_types, through: :layer_product_types
  has_many :products, through: :layer_products
  has_many :product_manufacturers, through: :layer_product_manufacturers
  
  attr_accessible :name, :note
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_relation_type:
        zero:  "Типов отношений объекта к слою"
        one:   "Тип отношения объекта к слою"
        few:   "Типа отношений объекта к слою"
        many:  "Типов отношений объекта к слою"
        other: "Типов отношений объекта к слою"
    attributes:
      layer_relation_type:
        name:              "Наименование"
        note:              "Примечание"
        layer_product_categories:    "Связки к подчиненным категориям товаров"
        layer_product_params:        "Связки к подчиненным типам параметров"
        layer_product_types:         "Связки к подчиненным типам товаров"
        layer_products:              "Связки к подчиненным товарам"
        layer_product_manufacturers: "Связки к подчиненным производителям"
        product_categories:          "Подчиненные категории товаров"
        product_param_types:         "Подчиненные типы параметров"
        product_types:               "Подчиненные типы товаров"
        products:                    "Подчиненные товары"
        product_manufacturers:       "Подчиненные производители"
  placeholder:
      layer_relation_type:
        name:                        "Введите наименование"
        note:                        "Введите примечание"
        layer_product_categories:    "Добавьте связку к подчиненным категориям товаров"
        layer_product_params:        "Добавьте связку к подчиненным типам параметров"
        layer_product_types:         "Добавьте связку к подчиненным типам товаров"
        layer_products:              "Добавьте связку к подчиненным товарам"
        layer_product_manufacturers: "Добавьте связку к подчиненным производителям"
        product_categories:          "Добавьте подчиненную категорию товаров"
        product_param_types:         "Добавьте подчиненный тип параметров"
        product_types:               "Добавьте подчиненный тип товаров"
        products:                    "Добавьте подчиненный товар"
        product_manufacturers:       "Добавьте подчиненного производителя"
  confirm_delete:
    layer_relation_type: 'Вы действительно хотите удалить тип отношения объект-слой %{name}?'
STR
