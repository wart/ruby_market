# encoding: utf-8
class LayerProduct < ActiveRecord::Base
    belongs_to :layer, inverse_of: :layer_products
    belongs_to :product, inverse_of: :layer_products
    belongs_to :layer_prod_relation_type, inverse_of: :layer_products
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_product:
        zero:  "Связок слой-товар"
        one:   "Связка слой-товар"
        few:   "Связки слой-товар"
        many:  "Связок слой-товар"
        other: "Связок слой-товар"
    attributes:
      layer_product:
        layer:                    "Слой"
        product:                  "Товар"
        layer_prod_relation_type: "Тип отношения"
  placeholder:
      layer_product:
        layer:                    "Выберите cлой"
        product:                  "Выберите товар"
        layer_prod_relation_type: "Выберите тип отношения"
  confirm_delete:
    layer_product: 'Вы действительно хотите удалить связку?'
STR
