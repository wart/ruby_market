# encoding: utf-8
class LayerProductCategory < ActiveRecord::Base
    belongs_to :layer, inverse_of: :layer_product_categories
    belongs_to :product_category, inverse_of: :layer_product_categories
    belongs_to :layer_prod_relation_type, inverse_of: :layer_product_categories
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_product_category:
        zero:  "Связок слой-категория товара"
        one:   "Связка слой-категория товара"
        few:   "Связки слой-категория товара"
        many:  "Связок слой-категория товара"
        other: "Связок слой-категория товара"
    attributes:
      layer_product_category:
        layer:                    "Слой"
        product_category:         "Категория товара"
        layer_prod_relation_type: "Тип отношения"
  placeholder:
      layer_product_category:
        layer:                    "Выберите cлой"
        product_category:         "Выберите категорию товара"
        layer_prod_relation_type: "Выберите тип отношения"
  confirm_delete:
    layer_product_category: 'Вы действительно хотите удалить связку?'
STR
