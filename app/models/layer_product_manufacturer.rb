# encoding: utf-8
class LayerProductManufacturer < ActiveRecord::Base
    belongs_to :layer, inverse_of: :layer_product_manufacturers
    belongs_to :product_manufacturer, inverse_of: :layer_product_manufacturers
    belongs_to :layer_prod_relation_type, inverse_of: :layer_product_manufacturers
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_product_manufacturer:
        zero:  "Связок слой-производитель товара"
        one:   "Связка слой-производитель товара"
        few:   "Связки слой-производитель товара"
        many:  "Связок слой-производитель товара"
        other: "Связок слой-производитель товара"
    attributes:
      layer_product_manufacturer:
        layer:                    "Слой"
        product_manufacturer:     "Производитель товара"
        layer_prod_relation_type: "Тип отношения"
  placeholder:
      layer_product_manufacturer:
        layer:                    "Выберите cлой"
        product_manufacturer:     "Выберите производителя товара"
        layer_prod_relation_type: "Выберите тип отношения"
  confirm_delete:
    layer_product_manufacturer: 'Вы действительно хотите удалить связку?'
STR