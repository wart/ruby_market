# encoding: utf-8
class LayerProductParam < ActiveRecord::Base
    belongs_to :layer, inverse_of: :layer_product_params
    belongs_to :product_param_type, inverse_of: :layer_product_params
    belongs_to :layer_prod_relation_type, inverse_of: :layer_product_params
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_product_param:
        zero:  "Связок слой-тип параметра товара"
        one:   "Связка слой-тип параметра товара"
        few:   "Связки слой-тип параметра товара"
        many:  "Связок слой-тип параметра товара"
        other: "Связок слой-тип параметра товара"
    attributes:
      layer_product_param:
        layer:                    "Слой"
        product_param_type:       "Тип параметра товара"
        layer_prod_relation_type: "Тип отношения"
  placeholder:
      layer_product_param:
        layer:                    "Выберите cлой"
        product_param_type:       "Выберите тип параметра товара"
        layer_prod_relation_type: "Выберите тип отношения"
  confirm_delete:
    layer_product_param: 'Вы действительно хотите удалить связку?'
STR
