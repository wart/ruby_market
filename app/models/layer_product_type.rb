# encoding: utf-8
class LayerProductType < ActiveRecord::Base
  
    belongs_to :layer, inverse_of: :layer_product_types
    belongs_to :product_type, inverse_of: :layer_product_types
    belongs_to :layer_prod_relation_type, inverse_of: :layer_product_types
    
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_product_type:
        zero:  "Связок слой-тип товара"
        one:   "Связка слой-тип товара"
        few:   "Связки слой-тип товара"
        many:  "Связок слой-тип товара"
        other: "Связок слой-тип товара"
    attributes:
      layer_product_type:
        layer:                    "Слой"
        product_type:             "Тип товара"
        layer_prod_relation_type: "Тип отношения"
  placeholder:
      layer_product_type:
        layer:                    "Выберите cлой"
        product_type:             "Выберите тип товара"
        layer_prod_relation_type: "Выберите тип отношения"
  confirm_delete:
    layer_product_type: 'Вы действительно хотите удалить связку?'
STR
