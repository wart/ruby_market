# encoding: utf-8
class LayerRelation < ActiveRecord::Base
  belongs_to :layer_relation_type, inverse_of: :layer_relations
  belongs_to :parent, class_name: "Layer", inverse_of: :child_links
  belongs_to :child , class_name: "Layer", inverse_of: :parent_links
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_relation:
        zero:  "Отношений слоев"
        one:   "Отношение слоев"
        few:   "Отношения слоев"
        many:  "Отношений слоев"
        other: "Отношений слоев"
    attributes:
      layer_relation:
        layer_relation_type: "Тип отношения"
        parent:              "Предок"
        child:               "Потомок"
  placeholder:
      layer_relation:
        layer_relation_type: "Выберите Тип отношения"
        parent:              "Выберите Предок"
        child:               "Выберите Потомок"
  confirm_delete:
    layer_relation: 'Вы действительно хотите удалить отношение?'
STR
