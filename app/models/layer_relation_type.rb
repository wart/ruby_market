# encoding: utf-8
class LayerRelationType < ActiveRecord::Base
  has_many :layer_relations, inverse_of: :layer_relation_type
  has_many :parents , class_name: "Layer", through: :layer_relations
  has_many :childs  , class_name: "Layer", through: :layer_relations
  attr_accessible :name, :note
end
append_translations <<STR
ru:
  activerecord:
    models:
      layer_relation_type:
        zero:  "Типов отношений"
        one:   "Тип отношения"
        few:   "Типа отношений"
        many:  "Типов отношений"
        other: "Типов отношений"
    attributes:
      layer_relation_type:
        name:              "Наименование"
        note:              "Примечание"
        layer_relations:   "Отношения слоев"
        parents:           "Предки"
        childs:            "потомки"
  placeholder:
      layer_relation_type:
        name:              "Введите наименование"
        note:              "Введите примечание"
        layer_relations:   "Добавьте отношения слоев"
        parents:           "Добавьте предка"
        childs:            "Добавьте потомка"
  confirm_delete:
    layer_relation_type: 'Вы действительно хотите удалить тип отношения %{name}?'
STR
