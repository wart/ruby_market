# encoding: utf-8
class Product < ActiveRecord::Base

  belongs_to :product_manufacturer, inverse_of: :products
  belongs_to :product_type, inverse_of: :products
  has_many :product_params, :dependent => :destroy, inverse_of: :product
  has_many :layer_products, :dependent => :destroy, inverse_of: :product
  has_many :layers,  through: :layer_products
end
append_translations <<STR
ru:
  activerecord:
    models:
      product:
        zero:  "товаров"
        one:   "товар"
        few:   "товара"
        many:  "товаров"
        other: "товаров"
    attributes:
      product:
        name:                 "Наименование"
        text:                 "Описание"
        product_manufacturer: "Производитель"
        product_type:         "Тип"
        product_params:       "Параметры"
        layer_products:       "Привязка к слоям"
        layers:               "Слои"
  placeholder:
      product:
        name:                 "Введите наименование"
        text:                 "Введите описание"
        product_manufacturer: "Выберите производителя"
        product_type:         "Выберите тип"
        product_params:       "Заполните Параметры"
        layer_products:       "Сделайте привязку к слоям"
        layers:               "Привяжите к слоям"
  confirm_delete:
    product: 'Вы действительно хотите удалить товар %{name}?'
STR