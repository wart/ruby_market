# encoding: utf-8
class ProductCategory < ActiveRecord::Base

  has_many   :childs, class_name: "ProductCategory",
                          foreign_key: "parent_id", inverse_of: :parent

  belongs_to :parent, class_name: "ProductCategory", inverse_of: :childs
  has_many   :product_types, inverse_of: :product_category

  has_many   :layer_product_categories, :dependent => :destroy, inverse_of: :product_category
  has_many   :layers,  through: :layer_product_categories
end

append_translations <<STR
ru:
  activerecord:
    models:
      product_category:
        zero:  "Категорий товаров"
        one:   "Категория товаров"
        few:   "Категории товаров"
        many:  "Категорий товаров"
        other: "Категорий товаров"
    attributes:
      product_category:
        name:                     "Наименование"
        shortname:                "Короткое наименование"      
        parent:                   "Родитель"
        childs:                   "потомки"
        product_types:            "Тип товаров"
        layer_product_categories: "Привязки к слоям"
        layers:                   "Слои"
  placeholder:
      product_category:
        name:                     "Введите наименование"
        shortname:                "Введите короткое наименование"      
        parent:                   "Выберите родителя"
        childs:                   "Добавьте потомков"
        product_types:            "Добавьте типы товаров"
        layer_product_categories: "Создайте привязку к слоям"
        layers:                   "Привяжите к слоям"
  confirm_delete:
    product_category: 'Вы действительно хотите удалить категорию %{name}?'
STR
