# encoding: utf-8
class ProductManLinkType < ActiveRecord::Base
  belongs_to :product_type, inverse_of: :product_man_link_types 
  belongs_to :product_manufacturer, inverse_of: :product_man_link_types
  
  attr_accessible :product_manufacturer_id, :product_type_id
   
end
append_translations <<STR
ru:
  activerecord:
    models:
      product_man_link_type:
        zero:  "Связок типа товара и производителя"
        one:   "Связка типа товара и производителя"
        few:   "Связки типа товара и производителя"
        many:  "Связок типа товара и производителя"
        other: "Связок типа товара и производителя"
    attributes:
      product_man_link_type:
        product_manufacturer: "Производитель"
        product_type:         "Тип товаров"
  placeholder:
      product_man_link_type:
        product_manufacturer: "Выберите производителя"
        product_type:         "Выберите тип товаров"
  confirm_delete:
    product_man_link_type: 'Вы действительно хотите удалить связку?'
STR
