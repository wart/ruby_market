# encoding: utf-8
class ProductManufacturer < ActiveRecord::Base
  
  has_many :products, :dependent => :destroy, inverse_of: :product_manufacturer
  has_many :product_man_link_types, inverse_of: :product_manufacturer
  has_many :product_types, through: :product_man_link_types  
  
  has_many :layer_product_manufacturers, :dependent => :destroy, inverse_of: :product_manufacturer
  has_many :layers,  through: :layer_product_manufacturers
  attr_accessible :name, :note
end
append_translations <<STR
ru:
  activerecord:
    models:
      product_manufacturer:
        zero:  "производителей"
        one:   "производитель"
        few:   "производителя"
        many:  "производителей"
        other: "производителей"
    attributes:
      product_manufacturer:
        name:                        "Наименование"
        note:                        "Примечание"
        products:                    "Товары"
        product_man_link_types:      "Привязка к типам товаров"
        product_types:               "Типы товаров"
        layer_product_manufacturers: "Привязка к слоям"
        layers:                      "Слои"
  placeholder:
      product_manufacturer:
        name:                        "Введите наименование"
        note:                        "Введите примечание"
        products:                    "Создайте товары"
        product_man_link_types:      "Сделайте привязку к типам товаров"
        product_types:               "Привяжите к типам товаров"
        layer_product_manufacturers: "Сделайте привязку к слоям"
        layers:                      "Привяжите к слоям"
  confirm_delete:
    product_manufacturer: 'Вы действительно хотите удалить производителя %{name}?'
STR
