# encoding: utf-8
class ProductParam < ActiveRecord::Base

  belongs_to :product_param_type, inverse_of:  :product_params
  belongs_to :product, inverse_of: :product_params
  
end
append_translations <<STR
ru:
  activerecord:
    models:
      product_param:
        zero:  "параметров"
        one:   "параметр"
        few:   "параметра"
        many:  "параметров"
        other: "параметров"
    attributes:
      product_param:
        product:            "Товар"
        product_param_type: "Тип параметра"
        val:                "Значение"
  placeholder:
      product_param:
        product:            "Выберите товар"
        product_param_type: "Выберите тип параметра"
        val:                "Введите значение"
  confirm_delete:
    product_param: 'Вы действительно хотите удалить параметр товара?'
STR
