# encoding: utf-8
class ProductParamCategory < ActiveRecord::Base
  has_many :product_params_types, :dependent => :destroy, inverse_of: :product_param_category
  has_many :product_type_vs_param_cats, :dependent => :destroy, inverse_of: :product_param_category
  has_many :product_types, through: :product_type_vs_param_cats
  attr_accessible :name, :note
end
append_translations <<STR
ru:
  activerecord:
    models:
      product_param_category:
        zero:  "категорий типов параметров"
        one:   "категория типов параметров"
        few:   "категории типов параметров"
        many:  "категорий типов параметров"
        other: "категорий типов параметров"
    attributes:
      product_param_category:
        name:                       "Наименование"
        note:                       "Примечание"
        product_params_types:       "Типы параметров"
        product_type_vs_param_cats: "Привязка к типам товаров"
        product_types:              "Типы товаров"
  placeholder:
      product_param_category:
        name:                       "Введите наименование"
        note:                       "Введите примечание"
        product_params_types:       "Добавте типы параметров"
        product_type_vs_param_cats: "Создайте привязку к типам товаров"
        product_types:              "Привяжите к типам товаров"
  confirm_delete:
    product_param_category: 'Вы действительно хотите удалить категорию %{name}?'
STR
          