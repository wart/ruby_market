# encoding: utf-8
class ProductParamType < ActiveRecord::Base
  belongs_to :product_param_category , inverse_of: :product_params_types
  has_many   :product_params, :dependent => :destroy, inverse_of: :product_param_type
  has_many   :layer_product_params, :dependent => :destroy, inverse_of: :product_param_type
  has_many   :layers,  through: :layer_product_params
end
append_translations <<STR
ru:
  activerecord:
    models:
      product_param_type:
        zero:  "типов параметров"
        one:   "тип параметров"
        few:   "типа параметров"
        many:  "типов параметров"
        other: "типов параметров"
    attributes:
      product_param_type:
        name:                   "Наименование"
        note:                   "Примечание"
        is_main:                "Приоритетный"
        product_param_category: "Категория параметра"
        product_params:         "Параметры продуктов"
        layer_product_params:   "Привязка к слоям"
        layers:                 "Слои"
  placeholder:
      product_param_type:
        name:                   "Введите Наименование"
        note:                   "Введите Примечание"
        is_main:                "Выберите приоритетность"
        product_param_category: "Выберите категорию параметра"
        product_params:         "Выберите параметры продуктов"
        layer_product_params:   "Создайте привязку к слоям"
        layers:                 "Привяжите к слоям"
  confirm_delete:
    product_param_type: 'Вы действительно хотите удалить тип параметров %{name}?'
STR
