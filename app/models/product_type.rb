# encoding: utf-8
class ProductType < ActiveRecord::Base

  belongs_to :product_category, inverse_of: :product_types
  
  belongs_to :parent, class_name: "ProductType", inverse_of: :childs
  has_many :childs  , class_name: "ProductType", foreign_key: "parent_id", inverse_of: :parent
                      
  has_many :products, inverse_of: :product_type
  has_many :product_man_link_types, :dependent => :destroy, inverse_of: :product_type
  has_many :product_manufacturers, through: :product_man_link_types  
  has_many :product_type_vs_param_cats, :dependent => :destroy, inverse_of: :product_type
  has_many :product_param_categories, through: :product_type_vs_param_cats
  has_many :layer_product_types  , :dependent => :destroy, inverse_of: :product_type
  has_many :layers,  through: :layer_product_types

end
append_translations <<STR
ru:
  activerecord:
    models:
      product_type:
        zero:  "типов"
        one:   "тип"
        few:   "типа"
        many:  "типов"
        other: "типов"
    attributes:
      product_type:
        name:                       "Наименование"
        note:                       "Примечание"
        product_category:           "Категория"
        parent:                     "Родитель"
        childs:                     "Потомки"
        products:                   "Товары"
        product_man_link_types:     "Привязка к производителям"
        product_manufacturers:      "Производители"
        product_type_vs_param_cats: "Привязка к категориям параметров"
        product_param_categories:   "Категории параметров"
        layer_product_types:        "Привязка к слоям"
        layers:                     "Слои"
  placeholder:
      product_type:
        name:                       "Введите наименование"
        note:                       "Введите примечание"
        product_category:           "Выберите категорию"
        parent:                     "Выберите родителя"
        childs:                     "Добавьте потомков"
        products:                   "Добавьте товары"
        product_man_link_types:     "Создайте привязку к производителям"
        product_manufacturers:      "Привяжите к производителям"
        product_type_vs_param_cats: "Создайте привязку к категориям параметров"
        product_param_categories:   "Привяжите к категориям параметров"
        layer_product_types:        "Создайте привязку к слоям"
        layers:                     "Привяжите к слоям"
  confirm_delete:
    product_type: 'Вы действительно хотите удалить тип товара %{name}?'
STR
