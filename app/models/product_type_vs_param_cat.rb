# encoding: utf-8
class ProductTypeVsParamCat < ActiveRecord::Base
  belongs_to :product_param_category, inverse_of: :product_type_vs_param_cats
  belongs_to :product_type, inverse_of: :product_type_vs_param_cats
end
append_translations <<STR
ru:
  activerecord:
    models:
      product_type_vs_param_cat:
        zero:  "Связок типа товара и категории параметров"
        one:   "Связка типа товара и категории параметров"
        few:   "Связки типа товара и категории параметров"
        many:  "Связок типа товара и категории параметров"
        other: "Связок типа товара и категории параметров"
    attributes:
      product_type_vs_param_cat:
        product_param_category: "Категория параметров"
        product_type:           "Тип товаров"
  placeholder:
      product_type_vs_param_cat:
        product_param_category: "Выберите категорию параметров"
        product_type:           "Выберите тип товаров"
  confirm_delete:
    product_type_vs_param_cat: 'Вы действительно хотите удалить связку?'
STR
