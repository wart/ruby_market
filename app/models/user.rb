class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
#         :token_authenticatable
  
#  before_save :ensure_authentication_token

  
  
  has_many :users_roles
  has_many :roles, :through => :users_roles         
 
  before_create :create_role

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  
  def role?(role)
    return !!self.roles.find_by_name(role)
  end

  private
    def create_role
      self.roles << Role.find_by_name(:user)
    end
end
