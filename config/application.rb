require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module MagTempl
  class Application < Rails::Application
    
    # grape
    config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
    config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]
    
    #grape-rabl
    config.middleware.use(Rack::Config) do |env|
      env['api.tilt.root'] = Rails.root.join "app", "views", "api"
    end    
    
    
    # locales
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.default_locale = :ru
    config.i18n.locale = :ru    

    # validators
    #config.autoload_paths += %W(#{config.root}/app/models/validators)
    
    
    
    # paperclip 
    Paperclip::Attachment.default_options[:url] = "/system/:class/:attachment/:id/:style.:extension"
    
    #assets
    config.assets.enable=true
    
    if Rails.env.production?
      config.exceptions_app = self.routes
    end

  end
end
