def append_translations(src)
      YAML.load(src).each { |l,d| I18n.config.backend.store_translations(l,d||{})}
end    
