class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    t.belongs_to :product_type, :null => false, index: true
    t.belongs_to :product_manufacturer, :null => false, index: true
    t.string     :name,:null => false
    t.text       :text
    
    end
  end
end
