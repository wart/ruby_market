class CreateProductCategories < ActiveRecord::Migration
  def change
    create_table :product_categories do |t|
      t.references :parent
      t.string   :name,:null => false
      t.string   :shortname,:null => false
      
    end
  end
end
