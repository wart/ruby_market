class CreateProductParams < ActiveRecord::Migration
  def change
    create_table :product_params , id: false do |t|
      t.belongs_to :product, :null => false, index: true
      t.belongs_to :product_param_type, :null => false, index: true
      t.string     :val,:null => true
    end
    execute "ALTER TABLE product_params ADD PRIMARY KEY (product_id,product_param_type_id);"
  end
end
