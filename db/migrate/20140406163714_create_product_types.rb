class CreateProductTypes < ActiveRecord::Migration
  def change
    create_table :product_types do |t|
      t.references :parent
      t.references :product_category
      t.string     :name,:null => false
      t.text       :note      
    end
  end
end
