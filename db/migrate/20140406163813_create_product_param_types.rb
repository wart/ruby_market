class CreateProductParamTypes < ActiveRecord::Migration
  def change
    create_table :product_param_types do |t|
      t.belongs_to :product_param_category, :null => false, index: true
      t.string     :name,:null => false
      t.boolean    :is_main,:null => false
      t.text       :note,:null => false
    end
  end
end
