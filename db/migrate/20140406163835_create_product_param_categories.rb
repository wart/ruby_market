class CreateProductParamCategories < ActiveRecord::Migration
  def change
    create_table :product_param_categories do |t|
      t.string     :name,:null => false
      t.text       :note,:null => true      
    end
  end
end
