class CreateProductManufacturers < ActiveRecord::Migration
  def change
    create_table :product_manufacturers do |t|

      t.string     :name,:null => false
      t.text       :note,:null => true
      
    end
  end
end
