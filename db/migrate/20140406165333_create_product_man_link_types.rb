class CreateProductManLinkTypes < ActiveRecord::Migration
  def change
    create_table :product_man_link_types , id: false do |t|
      t.belongs_to :product_manufacturer, :null => false, index: true
      t.belongs_to :product_type, :null => false, index: true
    end
    execute "ALTER TABLE product_man_link_types ADD PRIMARY KEY (product_manufacturer_id,product_type_id);"
    
  end
end
