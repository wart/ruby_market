class CreateProductTypeVsParamCats < ActiveRecord::Migration
  def change
    create_table :product_type_vs_param_cats, id: false do |t|
      t.belongs_to :product_param_category, :null => false, index: true
      t.belongs_to :product_type, :null => false, index: true
    end
    execute "ALTER TABLE product_type_vs_param_cats ADD PRIMARY KEY (product_param_category_id,product_type_id);"
  end
end
