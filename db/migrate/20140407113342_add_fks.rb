class AddFks < ActiveRecord::Migration
  def change
   
     execute "ALTER TABLE product_categories ADD CONSTRAINT fk_product_categories   FOREIGN KEY (parent_id) REFERENCES product_categories(id)"
     execute "ALTER TABLE product_types ADD CONSTRAINT fk_product_type_cat   FOREIGN KEY (product_category_id) REFERENCES product_categories(id)"
     execute "ALTER TABLE product_types ADD CONSTRAINT fk_product_types   FOREIGN KEY (parent_id) REFERENCES product_types(id)"
     execute "ALTER TABLE product_man_link_types ADD CONSTRAINT fk_product_man_link_types_man   FOREIGN KEY (product_manufacturer_id) REFERENCES product_manufacturers(id)"
     execute "ALTER TABLE product_man_link_types ADD CONSTRAINT fk_product_man_link_types_typ   FOREIGN KEY (product_type_id) REFERENCES product_types(id)"
     execute "ALTER TABLE product_type_vs_param_cats ADD CONSTRAINT fk_product_type_vs_param_cats_typ   FOREIGN KEY (product_param_category_id) REFERENCES product_param_categories(id)"
     execute "ALTER TABLE product_type_vs_param_cats ADD CONSTRAINT fk_product_type_vs_param_cats_cat   FOREIGN KEY (product_type_id) REFERENCES product_types(id)"
     execute "ALTER TABLE products ADD CONSTRAINT fk_products_product_types   FOREIGN KEY (product_type_id) REFERENCES product_types(id)"
     execute "ALTER TABLE products ADD CONSTRAINT fk_products_product_manufacturers   FOREIGN KEY (product_manufacturer_id) REFERENCES product_manufacturers(id)"
     execute "ALTER TABLE product_params ADD CONSTRAINT fk_product_params_products   FOREIGN KEY (product_id) REFERENCES products(id)"
     execute "ALTER TABLE product_params ADD CONSTRAINT fk_product_params_param_types FOREIGN KEY (product_param_type_id) REFERENCES product_param_types(id)"
     execute "ALTER TABLE product_param_types ADD CONSTRAINT fk_product_param_types_cat   FOREIGN KEY (product_param_category_id) REFERENCES product_param_categories(id)"
    
  end
end
