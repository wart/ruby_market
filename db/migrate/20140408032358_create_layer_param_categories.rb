class CreateLayerParamCategories < ActiveRecord::Migration
  def change
    create_table :layer_param_categories do |t|
      t.string   :name,:null => false
      t.text     :note,:null => true
    end
  end
end
