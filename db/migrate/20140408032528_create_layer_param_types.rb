class CreateLayerParamTypes < ActiveRecord::Migration
  def change
    create_table :layer_param_types do |t|
      t.belongs_to :layer_param_category, :null => false, index: true
      t.string     :name,:null => false
      t.text       :note,:null => true
    end
  end
end
