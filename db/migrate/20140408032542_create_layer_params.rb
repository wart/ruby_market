class CreateLayerParams < ActiveRecord::Migration
  def change
    create_table :layer_params, id: false do |t|
      t.belongs_to :layer, :null => false, index: true
      t.belongs_to :layer_param_type, :null => false, index: true
      t.string     :val,:null => true
     end
     execute "ALTER TABLE layer_params ADD PRIMARY KEY (layer_id,layer_param_type_id);"
 end
end
