class CreateLayerProductTypes < ActiveRecord::Migration
  def change
    create_table :layer_product_types, id: false do |t|
      t.belongs_to :layer, :null => false, index: true
      t.belongs_to :layer_prod_relation_type, :null => false, index: true
      t.belongs_to :product_type, :null => false, index: true
    end
     execute "ALTER TABLE layer_product_types ADD PRIMARY KEY (layer_id,layer_prod_relation_type_id,product_type_id);"
  end
end
