class CreateLayerProductCategories < ActiveRecord::Migration
  def change
    create_table :layer_product_categories, id: false do |t|
      t.belongs_to :layer, :null => false, index: true
      t.belongs_to :layer_prod_relation_type, :null => false, index: true
      t.belongs_to :product_category, :null => false, index: true
    end
    execute "ALTER TABLE layer_product_categories ADD PRIMARY KEY (layer_id,layer_prod_relation_type_id,product_category_id);"
  end
end
