class CreateLayerRelations < ActiveRecord::Migration
  def change
    create_table :layer_relations, id: false do |t|
      t.references :parent
      t.references :child
      t.references :layer_relation_type
    end
    execute "ALTER TABLE layer_relations ADD PRIMARY KEY (parent_id,child_id,layer_relation_type_id);"
  end
end
