class CreateLayerProductParams < ActiveRecord::Migration
  def change
    create_table :layer_product_params, id: false do |t|
      t.belongs_to :layer, :null => false, index: true
      t.belongs_to :layer_prod_relation_type, :null => false, index: true
      t.belongs_to :product_param_type, :null => false, index: true
      t.boolean    :by_val,:null => true
      t.text       :val,:null => true
    end
    execute "ALTER TABLE layer_product_params ADD PRIMARY KEY (layer_id,layer_prod_relation_type_id,product_param_type_id);"
  end
end
