class CreateLayerRelationTypes < ActiveRecord::Migration
  def change
    create_table :layer_relation_types do |t|
      t.string     :name,:null => false
      t.text       :note,:null => true
    end
  end
end
