class CreateLayerProductManufacturers < ActiveRecord::Migration
  def change
    create_table :layer_product_manufacturers, id: false do |t|
      t.belongs_to :layer, :null => false, index: true
      t.belongs_to :layer_prod_relation_type, :null => false, index: true
      t.belongs_to :product_manufacturer, :null => false, index: true
    end
    execute "ALTER TABLE layer_product_manufacturers ADD PRIMARY KEY (layer_id,layer_prod_relation_type_id,product_manufacturer_id);"
     
  end
end
