class AddLayerTablesFKs < ActiveRecord::Migration
  def change
    execute "ALTER TABLE  layer_param_types  ADD CONSTRAINT fk_layer_param_types_cat FOREIGN KEY (layer_param_category_id) REFERENCES layer_param_categories(id)"

    execute "ALTER TABLE  layer_params  ADD CONSTRAINT fk_layer_params_layer FOREIGN KEY (layer_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_params  ADD CONSTRAINT fk_layer_params_param_type FOREIGN KEY (layer_param_type_id) REFERENCES layer_param_types(id)"

    execute "ALTER TABLE  layer_relations  ADD CONSTRAINT fk_layer_relations_parent FOREIGN KEY (parent_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_relations  ADD CONSTRAINT fk_layer_relations_child FOREIGN KEY (child_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_relations  ADD CONSTRAINT fk_layer_relations_type FOREIGN KEY (layer_relation_type_id) REFERENCES layer_relation_types(id)"

    execute "ALTER TABLE  layer_products  ADD CONSTRAINT fk_layer_products_layer FOREIGN KEY (layer_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_products  ADD CONSTRAINT fk_layer_products_rel FOREIGN KEY (layer_prod_relation_type_id) REFERENCES layer_prod_relation_types(id)"
    execute "ALTER TABLE  layer_products  ADD CONSTRAINT fk_layer_products_product FOREIGN KEY (product_id) REFERENCES products(id)"

    execute "ALTER TABLE  layer_product_types  ADD CONSTRAINT fk_layer_product_types_layer FOREIGN KEY (layer_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_product_types  ADD CONSTRAINT fk_layer_product_types_rel FOREIGN KEY (layer_prod_relation_type_id) REFERENCES layer_prod_relation_types(id)"
    execute "ALTER TABLE  layer_product_types  ADD CONSTRAINT fk_layer_product_types_type FOREIGN KEY (product_type_id) REFERENCES product_types(id)"

    execute "ALTER TABLE  layer_product_categories  ADD CONSTRAINT fk_layer_product_categories_layer FOREIGN KEY (layer_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_product_categories  ADD CONSTRAINT fk_layer_product_categories_rel FOREIGN KEY (layer_prod_relation_type_id) REFERENCES layer_prod_relation_types(id)"
    execute "ALTER TABLE  layer_product_categories  ADD CONSTRAINT fk_layer_product_categories_cat FOREIGN KEY (product_category_id) REFERENCES product_categories(id)"

    execute "ALTER TABLE  layer_product_params  ADD CONSTRAINT fk_layer_product_params_layer FOREIGN KEY (layer_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_product_params  ADD CONSTRAINT fk_layer_product_params_rel FOREIGN KEY (layer_prod_relation_type_id) REFERENCES layer_prod_relation_types(id)"
    execute "ALTER TABLE  layer_product_params  ADD CONSTRAINT fk_layer_product_params_type FOREIGN KEY (product_param_type_id) REFERENCES product_param_types(id)"

    execute "ALTER TABLE  layer_product_manufacturers  ADD CONSTRAINT fk_layer_product_manufacturers_layer FOREIGN KEY (layer_id) REFERENCES layers(id)"
    execute "ALTER TABLE  layer_product_manufacturers  ADD CONSTRAINT fk_layer_product_manufacturers_rel FOREIGN KEY (layer_prod_relation_type_id) REFERENCES layer_prod_relation_types(id)"
    execute "ALTER TABLE  layer_product_manufacturers  ADD CONSTRAINT fk_layer_product_manufacturers_man FOREIGN KEY (product_manufacturer_id) REFERENCES product_manufacturers(id)"
  end
end
