class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name
    end
    
    Role.create!(:name =>"mngr_product_type")
    Role.create!(:name =>"mngr_product_category")
    Role.create!(:name =>"mngr_product_manufacturer")
    Role.create!(:name =>"mngr_product_param_category")
    Role.create!(:name =>"mngr_product_param_type")
    Role.create!(:name =>"mngr_product_param")
    Role.create!(:name =>"mngr_product_man_link_type")
    Role.create!(:name =>"mngr_product_type_vs_param_cat")
    Role.create!(:name =>"mngr_product")
    Role.create!(:name =>"mngr_layer_param_category")
    Role.create!(:name =>"mngr_layer_param_type")
    Role.create!(:name =>"mngr_layer_param")
    Role.create!(:name =>"mngr_layer_relation_type")
    Role.create!(:name =>"mngr_layer_relation")
    Role.create!(:name =>"mngr_layer_prod_relation_type")
    Role.create!(:name =>"mngr_layer_product_category")
    Role.create!(:name =>"mngr_layer_product_manufacturer")
    Role.create!(:name =>"mngr_layer_product_param")
    Role.create!(:name =>"mngr_layer_product_type")
    Role.create!(:name =>"mngr_layer_product")
    Role.create!(:name =>"mngr_layer")
    Role.create!(:name =>"admin")
  end
end
