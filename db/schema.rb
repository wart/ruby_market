# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140417113946) do

  create_table "layer_param_categories", force: true do |t|
    t.string "name", null: false
    t.text   "note"
  end

  create_table "layer_param_types", force: true do |t|
    t.integer "layer_param_category_id", null: false
    t.string  "name",                    null: false
    t.text    "note"
  end

  add_index "layer_param_types", ["layer_param_category_id"], name: "index_layer_param_types_on_layer_param_category_id", using: :btree

  create_table "layer_params", id: false, force: true do |t|
    t.integer "layer_id",            null: false
    t.integer "layer_param_type_id", null: false
    t.string  "val"
  end

  add_index "layer_params", ["layer_id"], name: "index_layer_params_on_layer_id", using: :btree
  add_index "layer_params", ["layer_param_type_id"], name: "index_layer_params_on_layer_param_type_id", using: :btree

  create_table "layer_prod_relation_types", force: true do |t|
    t.string "name", null: false
    t.text   "note"
  end

  create_table "layer_product_categories", id: false, force: true do |t|
    t.integer "layer_id",                    null: false
    t.integer "layer_prod_relation_type_id", null: false
    t.integer "product_category_id",         null: false
  end

  add_index "layer_product_categories", ["layer_id"], name: "index_layer_product_categories_on_layer_id", using: :btree
  add_index "layer_product_categories", ["layer_prod_relation_type_id"], name: "index_layer_product_categories_on_layer_prod_relation_type_id", using: :btree
  add_index "layer_product_categories", ["product_category_id"], name: "index_layer_product_categories_on_product_category_id", using: :btree

  create_table "layer_product_manufacturers", id: false, force: true do |t|
    t.integer "layer_id",                    null: false
    t.integer "layer_prod_relation_type_id", null: false
    t.integer "product_manufacturer_id",     null: false
  end

  add_index "layer_product_manufacturers", ["layer_id"], name: "index_layer_product_manufacturers_on_layer_id", using: :btree
  add_index "layer_product_manufacturers", ["layer_prod_relation_type_id"], name: "index_layer_product_manufacturers_on_layer_prod_relation_type_id", using: :btree
  add_index "layer_product_manufacturers", ["product_manufacturer_id"], name: "index_layer_product_manufacturers_on_product_manufacturer_id", using: :btree

  create_table "layer_product_params", id: false, force: true do |t|
    t.integer "layer_id",                    null: false
    t.integer "layer_prod_relation_type_id", null: false
    t.integer "product_param_type_id",       null: false
    t.boolean "by_val"
    t.text    "val"
  end

  add_index "layer_product_params", ["layer_id"], name: "index_layer_product_params_on_layer_id", using: :btree
  add_index "layer_product_params", ["layer_prod_relation_type_id"], name: "index_layer_product_params_on_layer_prod_relation_type_id", using: :btree
  add_index "layer_product_params", ["product_param_type_id"], name: "index_layer_product_params_on_product_param_type_id", using: :btree

  create_table "layer_product_types", id: false, force: true do |t|
    t.integer "layer_id",                    null: false
    t.integer "layer_prod_relation_type_id", null: false
    t.integer "product_type_id",             null: false
  end

  add_index "layer_product_types", ["layer_id"], name: "index_layer_product_types_on_layer_id", using: :btree
  add_index "layer_product_types", ["layer_prod_relation_type_id"], name: "index_layer_product_types_on_layer_prod_relation_type_id", using: :btree
  add_index "layer_product_types", ["product_type_id"], name: "index_layer_product_types_on_product_type_id", using: :btree

  create_table "layer_products", id: false, force: true do |t|
    t.integer "layer_id",                    null: false
    t.integer "layer_prod_relation_type_id", null: false
    t.integer "product_id",                  null: false
  end

  add_index "layer_products", ["layer_id"], name: "index_layer_products_on_layer_id", using: :btree
  add_index "layer_products", ["layer_prod_relation_type_id"], name: "index_layer_products_on_layer_prod_relation_type_id", using: :btree
  add_index "layer_products", ["product_id"], name: "index_layer_products_on_product_id", using: :btree

  create_table "layer_relation_types", force: true do |t|
    t.string "name", null: false
    t.text   "note"
  end

  create_table "layer_relations", id: false, force: true do |t|
    t.integer "parent_id",              default: 0, null: false
    t.integer "child_id",               default: 0, null: false
    t.integer "layer_relation_type_id", default: 0, null: false
  end

  add_index "layer_relations", ["child_id"], name: "fk_layer_relations_child", using: :btree
  add_index "layer_relations", ["layer_relation_type_id"], name: "fk_layer_relations_type", using: :btree

  create_table "layers", force: true do |t|
    t.string "name", null: false
    t.text   "note"
  end

  create_table "product_categories", force: true do |t|
    t.integer "parent_id"
    t.string  "name",      null: false
    t.string  "shortname", null: false
  end

  add_index "product_categories", ["parent_id"], name: "fk_product_categories", using: :btree

  create_table "product_man_link_types", id: false, force: true do |t|
    t.integer "product_manufacturer_id", null: false
    t.integer "product_type_id",         null: false
  end

  add_index "product_man_link_types", ["product_manufacturer_id"], name: "index_product_man_link_types_on_product_manufacturer_id", using: :btree
  add_index "product_man_link_types", ["product_type_id"], name: "index_product_man_link_types_on_product_type_id", using: :btree

  create_table "product_manufacturers", force: true do |t|
    t.string "name", null: false
    t.text   "note"
  end

  create_table "product_param_categories", force: true do |t|
    t.string "name", null: false
    t.text   "note"
  end

  create_table "product_param_types", force: true do |t|
    t.integer "product_param_category_id", null: false
    t.string  "name",                      null: false
    t.boolean "is_main",                   null: false
    t.text    "note",                      null: false
  end

  add_index "product_param_types", ["product_param_category_id"], name: "index_product_param_types_on_product_param_category_id", using: :btree

  create_table "product_params", id: false, force: true do |t|
    t.integer "product_id",            null: false
    t.integer "product_param_type_id", null: false
    t.string  "val"
  end

  add_index "product_params", ["product_id"], name: "index_product_params_on_product_id", using: :btree
  add_index "product_params", ["product_param_type_id"], name: "index_product_params_on_product_param_type_id", using: :btree

  create_table "product_type_vs_param_cats", id: false, force: true do |t|
    t.integer "product_param_category_id", null: false
    t.integer "product_type_id",           null: false
  end

  add_index "product_type_vs_param_cats", ["product_param_category_id"], name: "index_product_type_vs_param_cats_on_product_param_category_id", using: :btree
  add_index "product_type_vs_param_cats", ["product_type_id"], name: "index_product_type_vs_param_cats_on_product_type_id", using: :btree

  create_table "product_types", force: true do |t|
    t.integer "parent_id"
    t.integer "product_category_id"
    t.string  "name",                null: false
    t.text    "note"
  end

  add_index "product_types", ["parent_id"], name: "fk_product_types", using: :btree
  add_index "product_types", ["product_category_id"], name: "fk_product_type_cat", using: :btree

  create_table "products", force: true do |t|
    t.integer "product_type_id",         null: false
    t.integer "product_manufacturer_id", null: false
    t.string  "name",                    null: false
    t.text    "text"
  end

  add_index "products", ["product_manufacturer_id"], name: "index_products_on_product_manufacturer_id", using: :btree
  add_index "products", ["product_type_id"], name: "index_products_on_product_type_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", force: true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users_roles", ["role_id"], name: "index_users_roles_on_role_id", using: :btree
  add_index "users_roles", ["user_id"], name: "index_users_roles_on_user_id", using: :btree

end
