# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

  Role.create(:name => :admin)
  Role.create(:name => :user)
  

  Role.create(:name => :mngr_product_type)
  Role.create(:name => :mngr_product_category)
  Role.create(:name => :mngr_product_manufacturer)
  Role.create(:name => :mngr_product_param_category)
  Role.create(:name => :mngr_product_param_type)
  Role.create(:name => :mngr_product_param)
  Role.create(:name => :mngr_product_man_link_type)
  Role.create(:name => :mngr_product_type_vs_param_cat)
  Role.create(:name => :mngr_product)
  Role.create(:name => :mngr_layer_param_category)
  Role.create(:name => :mngr_layer_param_type)
  Role.create(:name => :mngr_layer_param)
  Role.create(:name => :mngr_layer_relation_type)
  Role.create(:name => :mngr_layer_relation)
  Role.create(:name => :mngr_layer_prod_relation_type)
  Role.create(:name => :mngr_layer_product_category)
  Role.create(:name => :mngr_layer_product_manufacturer)
  Role.create(:name => :mngr_layer_product_param)
  Role.create(:name => :mngr_layer_product_type)
  Role.create(:name => :mngr_layer_product)
  Role.create(:name => :mngr_layer)
  

