desc "API Routes"
task :routes do
  Shop::API.routes.each do |api|
    method = api.route_method.ljust(10)
    path = api.route_path
    puts "     #{method} #{path}" 
  end
  puts ""
  puts "------------------------------------------------------------------"
  puts""
end